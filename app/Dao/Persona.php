<?php

namespace App\Dao;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    //
    protected $fillable = ['nombre','tipo_documento','num_documento','direccion','telefono','email'];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'condicion' => 'boolean',
    ];
    
}
