<?php

namespace App\Dao;

use Illuminate\Database\Eloquent\Model;

class Articulo extends Model
{
    protected $fillable = [
        'idcategoria','codigo','nombre','precio_venta','stock','descripcion','condicion'
    ];

    protected $casts = [
        'condicion' => 'boolean',
    ];
    
    public function categoria()
    {
        return $this->belongsTo('App\Dao\Categoria');
    }
}
