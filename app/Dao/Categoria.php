<?php

namespace App\Dao;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    //
    //protected $table = 'categorias';
    //protected $primaryKey = 'id';
    protected $fillable = ['nombre','descripcion','condicion'];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'condicion' => 'boolean',
    ];
    
    public function articulos()
    {
        return $this->hasMany('App\Dao\Articulo');
    }
}
